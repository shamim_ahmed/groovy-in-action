package edu.buet.cse.ch04

// first range
def range1 = 0..10
println range1
println range1.contains(0)
println range1.contains(5)
println range1.contains(10)
println range1.contains(-1)
println range1.contains(11)

// second range
def range2 = 0..<10
println range2
println range2.contains(9)
println range2.contains(10)

println range2 instanceof Range
println range2.getClass().getCanonicalName()

// third range
def range3 = new IntRange(0, 10)
println range3
println range3.contains(0)
println range3.contains(5)
println range3.contains(10)

// fourth range
def range4 = 0.0..1.0
println range4
println range4.contains(1.0)
println range4.containsWithinBounds(0.5)

// date range
def today = new Date()
def yesterday = today - 1
def range5 = yesterday..today
println range5
println range5.size()
println range5.contains(yesterday)
println range5.contains(today)

// character range
def range6 = 'a'..'c'
println range6
println range6.contains('b')
